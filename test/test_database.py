'''
Created on 24.02.2017

Test cases for database.

@author: ilya
'''
import unittest
from channel.common.database import Engine

mdb = Engine('channel-test')

new_board = "cars"

class GeneralTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' 
        Runs once in the beginning
        '''
        mdb.connect()

    @classmethod
    def tearDownClass(cls):
        '''
        Runs once in the end
        '''
        mdb.close()

    def setUp(self):
        '''
        Runs before every test method
        '''
        mdb.populate()

    def tearDown(self):
        '''
        Runs after every test method
        '''
        mdb.clear()

    def test_boards_collection_created(self):
        '''
        Checks if collection containing Boards is indeed created
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_boards_collection_created.__name__, self.test_boards_collection_created.__doc__)
        boards = mdb.mdb[mdb.db_name]['boards']
        boards_count = boards.find().count()
        self.assertEquals(boards_count, 3)

    def test_threads_collection_created(self):
        '''
        Checks if collection containing Threads is indeed created
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_threads_collection_created.__name__, self.test_threads_collection_created.__doc__)
        threads = mdb.mdb[mdb.db_name]['threads']
        threads_count = threads.find().count()
        self.assertEquals(threads_count, 5)

    def test_posts_collection_created(self):
        '''
        Checks if collection containing Posts is indeed created
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_posts_collection_created.__name__, self.test_posts_collection_created.__doc__)
        posts = mdb.mdb[mdb.db_name]['posts']
        posts_count = posts.find().count()
        self.assertEquals(posts_count, 12)


class BoardTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' 
        Runs once in the beginning
        '''
        mdb.connect()

    @classmethod
    def tearDownClass(cls):
        '''
        Runs once in the end
        '''
        mdb.close()

    def setUp(self):
        '''
        Runs before every test method
        '''
        mdb.populate()

    def tearDown(self):
        '''
        Runs after every test method
        '''
        mdb.clear()

    def test_board_create(self):
        '''
        Checks if Board could be created
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_board_create.__name__, self.test_board_create.__doc__)
        board_id = mdb.create_board(new_board)
        self.assertIsNotNone(board_id)

    def test_board_create_existing(self):
        '''
        Checks if attempt to create Board with existing name returns None
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_board_create_existing.__name__, self.test_board_create_existing.__doc__)
        board_id = mdb.create_board('dev')
        self.assertIsNone(board_id)

    def test_board_get(self):
        '''
        Checks if Board could be retrieved
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_board_get.__name__, self.test_board_get.__doc__)
        # Create board first
        board_id = mdb.create_board(new_board)
        board = mdb.get_board(new_board)
        self.assertEqual(board_id, board['_id'])

    def test_board_get_nonexistent(self):
        '''
        Checks if attempt to retrieve nonexistent Board raises NameError
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_board_get_nonexistent.__name__, self.test_board_get_nonexistent.__doc__)
        board = new_board
        # Try to delete a board, catches and assert the Error
        with self.assertRaises(NameError) as context:
            mdb.get_board(board)
        self.assertTrue("Board {0} is not found".format(board) in context.exception)

    def test_board_edit(self):
        '''
        Checks if Board could be modified
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_board_edit.__name__, self.test_board_edit.__doc__)
        # Create board first
        mdb.create_board(new_board)
        desc = "About cars"
        upd = mdb.edit_board(new_board, {"description": desc})
        self.assertTrue(upd)
        modified_board = mdb.get_board(new_board)
        self.assertEqual(modified_board["description"], desc) 

    def test_board_delete(self):
        '''
        Checks if Board could be removed
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_board_delete.__name__, self.test_board_delete.__doc__)
        if mdb.delete_board('news'):
            # There should be only two boards left
            boards = mdb.mdb[mdb.db_name]['boards']
            boards_count = boards.find().count()
            self.assertEquals(boards_count, 2)

    def test_board_delete_nonexistent(self):
        '''
        Checks if attempt to delete nonexistent Board raises NameError
        '''
        # NOTE: See file /sample/data/boards.json for sample data
        print '({0}),{1}'.format(self.test_board_delete_nonexistent.__name__, self.test_board_delete_nonexistent.__doc__)   
        board = new_board
        # Try to delete a board, catches and assert the Error
        with self.assertRaises(NameError) as context:
            mdb.delete_board(board)
        self.assertTrue("Board {0} is not found".format(board) in context.exception) 

class ThreadTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' 
        Runs once in the beginning
        '''
        mdb.connect()

    @classmethod
    def tearDownClass(cls):
        '''
        Runs once in the end
        '''
        mdb.close()

    def setUp(self):
        '''
        Runs before every test method
        '''
        mdb.populate()

    def tearDown(self):
        '''
        Runs after every test method
        '''
        mdb.clear()

    def test_thread_create(self):
        '''
        Checks if Thread could be created
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_thread_create.__name__, self.test_thread_create.__doc__)
        #Create board first
        mdb.create_board(new_board)
        #Create thread to the board
        thread_id = mdb.create_thread(new_board, dict(title='BMW vs Mercedes'))
        self.assertIsNotNone(thread_id)

    def test_thread_create_nonexistent(self):
        '''
        Checks if attempt to create Thread under nonexistent Board raises NameError
        '''
        print '({0}),{1}'.format(self.test_thread_create_nonexistent.__name__, self.test_thread_create_nonexistent.__doc__)
        board = new_board
        # Try to create a thread in nonexistent board, catches and assert the Error
        with self.assertRaises(NameError) as context:
             mdb.create_thread(board, dict(title='BMW vs Mercedes'))
        self.assertTrue("Board {0} is not found".format(board) in context.exception)

    def test_thread_get(self):
        '''
        Checks if Thread could be retrieved
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_thread_get.__name__, self.test_thread_get.__doc__)
        # Create board first
        mdb.create_board(new_board)
        #Create thread
        thread_id = mdb.create_thread(new_board, dict(title='BMW vs Mercedes'))
        thread = mdb.get_thread(thread_id)
        self.assertEqual(thread_id, thread["_id"])

    def test_thread_get_nonexistent(self):
        '''
        Checks if attempt to get a nonexistent Thread with proper id raises NameError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_thread_get_nonexistent.__name__, self.test_thread_get_nonexistent.__doc__)

        # Creates a fake id
        thread_id = '53cb6b9b4f4ddef1ad47f943'
        # Use the thread_id to test it
        with self.assertRaises(NameError) as context:
            mdb.get_thread(thread_id)
        self.assertTrue('Thread is not found' in context.exception)

    def test_thread_get_wrongParam(self):
        '''
        Checks if attempt to get a Thread with wrong param format raises TypeError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_thread_get_wrongParam.__name__, self.test_thread_get_wrongParam.__doc__)

        # Creates a fake id, with wrong format
        thread_id = 'this format is not a good format to have'
        # Use the thread_id to test it
        with self.assertRaises(TypeError) as context:
            mdb.get_thread(thread_id)
        self.assertTrue('param thread_id does not have the right type' in context.exception)

    def test_thread_delete(self):
        '''
        Checks if Thread could be removed
        Checks if attempt to retrieve Thread with wrong id raises NameError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_thread_delete.__name__, self.test_thread_delete.__doc__)
        #Create thread first
        mdb.create_board(new_board)
        thread_id = mdb.create_thread(new_board, dict(title='BMW vs Mercedes'))
        mdb.delete_thread(thread_id)

        # Use the deleted thread_id to test it
        # If the thread is not deleted, NameError is not raised
        with self.assertRaises(NameError) as context:
            mdb.get_thread(thread_id)
        self.assertTrue('Thread is not found' in context.exception)

    def test_thread_delete_wrongParam(self):
        '''
        Checks if attempt to delete a Thread with wrong param format raises TypeError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_thread_delete_wrongParam.__name__, self.test_thread_delete_wrongParam.__doc__)

        # Creates a fake id, with wrong format
        thread_id = 'this format is not a good format to have'
        # Use the thread_id to test it
        with self.assertRaises(TypeError) as context:
            mdb.delete_thread(thread_id)
        self.assertTrue('param thread_id does not have the right type' in context.exception)

class PostTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' 
        Runs once in the beginning
        '''
        mdb.connect()

    @classmethod
    def tearDownClass(cls):
        '''
        Runs once in the end
        '''
        mdb.close()

    def setUp(self):
        '''
        Runs before every test method
        '''
        mdb.populate()

    def tearDown(self):
        '''
        Runs after every test method
        '''
        mdb.clear()

    def test_post_create(self):
        '''
        Checks if Post could be created
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_post_create.__name__, self.test_post_create.__doc__)
        # Create board first
        mdb.create_board(new_board)
        # Create thread to the created board
        thread_id = mdb.create_thread(new_board, dict(title='BMW vs Mercedes'))
        post_id = mdb.create_post(thread_id, {
            "author": 'nickname',
            "text": 'message',
            "votes": 2,
            "references": [],
            "attachments": []
        })
        self.assertIsNotNone(post_id)

    def test_post_create_nonexistent(self):
        '''
        Checks if attempt to create Post under nonexistent Thread raises NameError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_post_create_nonexistent.__name__, self.test_post_create_nonexistent.__doc__)
        # Create thread to the created board
        thread_id = mdb.create_thread('dev', dict(title='Sample dev thread'))
        mdb.delete_thread(thread_id)
        # Checks if the NameError thrown matches a not found thread_id error
        with self.assertRaises(NameError) as context:
            mdb.create_post(thread_id, {
                "author": 'nickname',
                "text": 'message',
                "votes": 2,
                "references": [],
                "attachments": []}
            )
        self.assertTrue("Thread with _id {0} is not found".format(thread_id) in context.exception)

    def test_post_get(self):
        '''
        Checks if Post could be retrieved
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_post_get.__name__, self.test_post_get.__doc__)
        # Create board first
        mdb.create_board(new_board)
        # Create thread to the created board
        thread_id = mdb.create_thread(new_board, dict(title='BMW vs Mercedes'))
        post_id = mdb.create_post(thread_id, {
            "author": 'nickname',
            "text": 'message',
            "votes": 2,
            "references": [],
            "attachments": []
        })
        post = mdb.get_post(post_id)
        self.assertIsNotNone(post)

    def test_post_get_nonexistent(self):
        '''
        Checks if attempt to get a nonexistent Post with proper id raises NameError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_post_get_nonexistent.__name__, self.test_post_get_nonexistent.__doc__)

        # Creates a fake id
        post_id = '53cb6b9b4f4ddef1ad47f943'
        # Use the post_id to test it
        with self.assertRaises(NameError) as context:
            mdb.get_post(post_id)
        self.assertTrue('Post is not found' in context.exception)

    def test_post_get_wrongParam(self):
        '''
        Checks if attempt to get a Post with wrong param format raises TypeError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_post_get_nonexistent.__name__, self.test_post_get_nonexistent.__doc__)

        # Creates a fake id, with wrong format
        post_id = 'this format is not a good format to have'
        # Use the post_id to test it
        with self.assertRaises(TypeError) as context:
            mdb.get_post(post_id)
        self.assertTrue('param post_id does not have the right type' in context.exception)

    def test_post_delete(self):
        '''
        Checks if Post could be removed
        Checks if attempt to retrieve Post with wrong id raises NameError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_post_delete.__name__, self.test_post_delete.__doc__)
        # Create board first
        mdb.create_board(new_board)
        # Create thread to the created board
        thread_id = mdb.create_thread(new_board, dict(title='BMW vs Mercedes'))
        post_id = mdb.create_post(thread_id, {
            "author": 'nickname',
            "text": 'message',
            "votes": 2,
            "references": [],
            "attachments": []
        })
        mdb.delete_post(post_id)

        # Use the deleted post_id to test it
        # If the thread is not deleted, NameError is not raised
        with self.assertRaises(NameError) as context:
            mdb.get_post(post_id)
        self.assertTrue('Post is not found' in context.exception)

    def test_post_delete_wrongParam(self):
        '''
        Checks if attempt to delete a Post with wrong param format raises TypeError
        '''
        # NOTE: See file /sample/data/threads.json for sample data
        print '({0}),{1}'.format(self.test_post_delete_wrongParam.__name__, self.test_post_delete_wrongParam.__doc__)

        # Creates a fake id, with wrong format
        post_id = 'this format is not a good format to have'
        # Use the post_id to test it
        with self.assertRaises(TypeError) as context:
            mdb.delete_post(post_id)
        self.assertTrue('param post_id does not have the right type' in context.exception)