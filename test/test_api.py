'''
Created on 11.04.2017

Test cases for API.

@author: ilya
'''

import json
import flask
import unittest
import datetime

from bson.objectid import ObjectId

from channel.common.database import Engine
from channel.common.constants import *

from channel import app, api

from channel.resources.boards import Boards, Board
from channel.resources.threads import Threads, Thread
from channel.resources.posts import Posts, Post

mdb = Engine('channel-test')

# Enable testing mode for Flask
app.config['TESTING'] = True
# Utilize testing database
app.config.update({'Engine': mdb})


class GeneralTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        ''' 
        Runs once in the beginning
        '''
        mdb.connect()

    @classmethod
    def tearDownClass(cls):
        '''
        Runs once in the end
        '''
        mdb.close()

    def setUp(self):
        '''
        Runs before every test method
        '''
        mdb.populate()

    def tearDown(self):
        '''
        Runs after every test method
        '''
        mdb.clear()


class BoardsTestCase(GeneralTestCase):

    url = '/channel/api/boards/'
    dev = '/channel/api/boards/dev/'
    cars = '/channel/api/boards/cars/'
    correct_board_request = {
        "name": "cars",
        "description": "About cars"
    }
    existing_board_request = {
        "name": "dev"
    }
    wrong_board_request = {
        "lol": "test"
    }
    update_dev = {
        "description": "IT and development"
    }

    def test_url(self):
        '''
        Checks that the URL points to the right resource
        '''
        print '({0}),{1}'.format(self.test_url.__name__, self.test_url.__doc__)
        with app.test_request_context(self.url):
            rule = flask.request.url_rule
            view_point = app.view_functions[rule.endpoint].view_class
            self.assertEquals(view_point, Boards)

    def test_get_boards(self):
        '''
        Checks that GET Boards return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_get_boards.__name__, self.test_get_boards.__doc__)
        with app.test_client() as client:
            resp = client.get(self.url)
            self.assertEquals(resp.status_code, 200)
            data = json.loads(resp.data)

            # Check items
            boards = data['items']
            self.assertEquals(len(boards), 3)

            # Check controls
            controls = data["@controls"]
            self.assertIn("self", controls)
            self.assertIn("profile", controls)
            self.assertIn("channel:create-board", controls)

    def test_post_boards(self):
        '''
        Checks that POST Boards return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_post_boards.__name__, self.test_post_boards.__doc__)
        with app.test_request_context(self.url), app.test_client() as client:
            resp = client.post(api.url_for(Boards),
                                headers={"Content-Type": JSON},
                                data=json.dumps(self.correct_board_request)
                               )
            self.assertTrue(resp.status_code == 200)
            url = resp.headers.get("Location")
            self.assertIsNotNone(url)

    def test_post_boards_existing(self):
        '''
        Checks that POST Boards with existing name returns correct status code
        '''
        print '({0}),{1}'.format(self.test_post_boards_existing.__name__, self.test_post_boards_existing.__doc__)
        with app.test_request_context(self.url), app.test_client() as client:
            resp = client.post(api.url_for(Boards),
                                headers={"Content-Type": JSON},
                                data=json.dumps(self.existing_board_request)
                               )
            self.assertTrue(resp.status_code == 500)

    def test_post_boards_wrong(self):
        '''
        Checks that POST Boards with wrong arguments returns correct status code
        '''
        print '({0}),{1}'.format(self.test_post_boards_wrong.__name__, self.test_post_boards_wrong.__doc__)
        with app.test_request_context(self.url), app.test_client() as client:
            resp = client.post(api.url_for(Boards),
                                headers={"Content-Type": JSON},
                                data=json.dumps(self.wrong_board_request)
                               )
            self.assertTrue(resp.status_code == 400)

    def test_get_board(self):
        '''
        Checks that GET Board return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_get_board.__name__, self.test_get_board.__doc__)
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Board, name="dev"))
            self.assertEquals(resp.status_code, 200)
            data = json.loads(resp.data)

            # Check items
            board_name = data['name']
            self.assertEquals(board_name, "dev")

            # Check controls
            controls = data["@controls"]
            self.assertIn("self", controls)
            self.assertIn("profile", controls)
            self.assertIn("channel:boards-all", controls)
            self.assertIn("channel:edit-board", controls)
            self.assertIn("channel:delete", controls)
            self.assertIn("channel:threads-all", controls)

    def test_get_board_nonexistent(self):
        '''
        Checks that GET Board for nonexistent resource returns correct status code
        '''
        print '({0}),{1}'.format(self.test_get_board_nonexistent.__name__, self.test_get_board_nonexistent.__doc__)
        with app.test_request_context(self.cars), app.test_client() as client:
            resp = client.get(api.url_for(Board, name="cars"))
            self.assertEquals(resp.status_code, 404)

    def test_put_board(self):
        '''
        Checks that PUT Board return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_put_board.__name__, self.test_put_board.__doc__)
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.put(api.url_for(Board, name="dev"),
                                headers={"Content-Type": JSON},
                                data=json.dumps(self.update_dev)
                               )
            self.assertTrue(resp.status_code == 200)
            url = resp.headers.get("Location")
            self.assertIsNotNone(url)
            resp = client.get(url)
            data = json.loads(resp.data)
            self.assertEqual(data["description"], self.update_dev["description"])

    def test_delete_board(self):
        '''
        Checks that DELETE Board return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_delete_board.__name__, self.test_delete_board.__doc__)
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.delete(api.url_for(Board, name="dev"))
            self.assertTrue(resp.status_code == 200)
            resp = client.get(self.url)
            data = json.loads(resp.data)
             # Check items
            boards = data['items']
            self.assertEquals(len(boards), 2)

class ThreadsTestCase(GeneralTestCase):

    url = '/channel/api/boards/dev/threads/'
    dev = '/channel/api/boards/dev/threads/58f39c5263071b0ab0aebabf'
    not_thread = '/channel/api/boards/car/threads/58f39c5263071b0ab0aebabf'
    correct_thread_request = {
        "title": "cars",
    }
    wrong_thread_request = {
        "lol": "test"
    }
    update_dev = {
        "title": "New NodeJS"
    }

    def test_url(self):
        '''
        Checks that the URL points to the right resource
        '''
        print '({0}),{1}'.format(self.test_url.__name__, self.test_url.__doc__)
        with app.test_request_context(self.url):
            rule = flask.request.url_rule
            view_point = app.view_functions[rule.endpoint].view_class
            self.assertEquals(view_point, Threads)

    def test_get_threads(self):
        '''
        Checks that GET Threads return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_get_threads.__name__, self.test_get_threads.__doc__)

        # # Check that I receive status code 200
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Threads, name="dev"))
            self.assertEquals(resp.status_code, 200)
            data = json.loads(resp.data)

            # Check items
            threads = data['items']
            self.assertEquals(len(threads), 2)

            # Check controls
            controls = data["@controls"]
            self.assertIn("self", controls)
            self.assertIn("channel:create-thread", controls)
            self.assertIn("channel:board", controls)

    def test_post_threads(self):
        '''
        Checks that POST Threads return correct status code and data format
        '''

        print '({0}),{1}'.format(self.test_post_threads.__name__, self.test_post_threads.__doc__)
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.post(api.url_for(Threads, name="dev"),
                               headers={"Content-Type": JSON},
                               data=json.dumps(self.correct_thread_request)
                               )
            self.assertTrue(resp.status_code == 200)
            url = resp.headers.get("Location")
            self.assertIsNotNone(url)

    def test_post_threads_nonexisting(self):
        '''
        Checks that POST Threads to non existing board returns correct status code
        '''

        print '({0}),{1}'.format(self.test_post_threads_nonexisting.__name__, self.test_post_threads_nonexisting.__doc__)
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.post(api.url_for(Threads, name="cars"),
                               headers={"Content-Type": JSON},
                               data=json.dumps(self.correct_thread_request)
                               )
            self.assertTrue(resp.status_code == 404)

    def test_post_threads_wrong(self):
        '''
        Checks that POST Threads with wrong arguments returns correct status code
        '''
        print '({0}),{1}'.format(self.test_post_threads_wrong.__name__, self.test_post_threads_wrong.__doc__)
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.post(api.url_for(Threads, name="dev"),
                               headers={"Content-Type": JSON},
                               data=json.dumps(self.wrong_thread_request)
                               )
            self.assertTrue(resp.status_code == 400)

    def test_get_thread(self):
        '''
        Checks that GET Thread return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_get_thread.__name__, self.test_get_thread.__doc__)

        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        # Check that I receive status code 200
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Thread, name="dev", thread_id=thread_id))
            self.assertEquals(resp.status_code, 200)

    def test_get_thread_nonexistent(self):
        '''
        Checks that GET Thread for nonexistent resource returns correct status code
        '''
        print '({0}),{1}'.format(self.test_get_thread_nonexistent.__name__, self.test_get_thread_nonexistent.__doc__)

        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        mdb.delete_thread(thread_id)
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Thread, name="dev", thread_id=thread_id))
            self.assertEquals(resp.status_code, 404)

    def test_put_thread(self):
        '''
        Checks that PUT Thread return correct status code and data format
        '''
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.put(api.url_for(Thread, name="dev", thread_id=thread_id),
                                headers={"Content-Type": JSON},
                                data=json.dumps(self.update_dev)
                            )
            self.assertEquals(resp.status_code, 200)

    def test_delete_thread(self):
        '''
        Checks that DELETE Thread return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_delete_thread.__name__, self.test_delete_thread.__doc__)

        # Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))

        headers = {
            'Accept': 'application/vnd.mason+json'
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.delete(api.url_for(Thread, name="dev", thread_id=thread_id),
                                 headers=headers)
            self.assertTrue(resp.status_code == 200)

            self.assertRaises(NameError, mdb.get_thread, thread_id)

class PostsTestCase(GeneralTestCase):

    dev = '/channel/api/boards/dev/'
    url = '/channel/api/boards/dev/threads/58b08c489916d10f34f5f9d1/posts/'
    full_url = ''
    update_post = {
        'text': "I'm testing again",
        'author': 'Arthur 2.0'
    }
    update_post_400 = {
        'author': 'Arthur 2.0'
    }

    def test_url(self):
        '''
        Checks that the URL points to the right resource
        '''
        print '({0}),{1}'.format(self.test_url.__name__, self.test_url.__doc__)
        with app.test_request_context(self.url):
            rule = flask.request.url_rule
            view_point = app.view_functions[rule.endpoint].view_class
            self.assertEquals(view_point, Posts)


    def test_GET_posts(self):
        '''
        Checks that GET Posts return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_GET_posts.__name__, self.test_GET_posts.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": thread_id,
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        post_id = mdb.create_post(thread_id, post)

        # # Check that I receive status code 200
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Posts, name="dev", thread_id=thread_id))
            self.assertEquals(resp.status_code, 200)
            data = json.loads(resp.data)

            # Check items
            posts = data['items']
            self.assertEquals(len(posts), 1)

            # Check controls
            controls = data["@controls"]
            self.assertIn("self", controls)
            self.assertIn("channel:create-post", controls)
            self.assertIn("channel:thread", controls)

    def test_GET_posts_404(self):
        '''
        Checks that GET Posts return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_GET_posts_404.__name__, self.test_GET_posts_404.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))

        mdb.delete_thread(thread_id)

        # # Check that I receive status code 404
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Posts, name="dev", thread_id=thread_id))
            self.assertEquals(resp.status_code, 404)

    def test_POST_posts(self):
        '''
        Checks that POST Posts return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_POST_posts.__name__, self.test_POST_posts.__doc__)

        #Creates the threads I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": str(thread_id),
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": str(datetime.datetime.utcnow()),
            "edited_utc": str(datetime.datetime.utcnow()),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.post(api.url_for(Posts, name="dev", thread_id=thread_id),
                                headers={"Content-Type": JSON},
                                data=json.dumps(post)
                               )
            self.assertTrue(resp.status_code == 200)
            url = resp.headers.get("Location")
            self.assertIsNotNone(url)

    def test_POST_posts_400(self):
        '''
        Checks that POST Posts return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_POST_posts_400.__name__, self.test_POST_posts_400.__doc__)

        #Creates the threads I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": str(thread_id),
            "author": "Arthur",
            "created_utc": str(datetime.datetime.utcnow()),
            "edited_utc": str(datetime.datetime.utcnow()),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.post(api.url_for(Posts, name="dev", thread_id=thread_id),
                                headers={"Content-Type": JSON},
                                data=json.dumps(post)
                               )
            self.assertTrue(resp.status_code == 400)

    def test_POST_posts_404(self):
        '''
        Checks that POST Posts return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_POST_posts_404.__name__, self.test_POST_posts_404.__doc__)

        #Creates the threads I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": str(thread_id),
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": str(datetime.datetime.utcnow()),
            "edited_utc": str(datetime.datetime.utcnow()),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.post(api.url_for(Posts, name="dev", thread_id="58b08c489916d10f34f5f9d1"),
                                headers={"Content-Type": JSON},
                                data=json.dumps(post)
                               )
            self.assertTrue(resp.status_code == 404)


    def test_GET_post(self):
        '''
        Checks that GET post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_GET_post.__name__, self.test_GET_post.__doc__)
        
        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": thread_id,
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        post_id = mdb.create_post(thread_id, post)

        # Check that I receive status code 200
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Post, name="dev", thread_id=thread_id, post_id=post_id))
            self.assertEquals(resp.status_code, 200)
            data = json.loads(resp.data)

            # Check items
            post_text = data['text']
            self.assertEquals(post_text, "I'm testing")
            post_author = data['author']
            self.assertEquals(post_author, "Arthur")

            # Check controls
            controls = data["@controls"]
            self.assertIn("self", controls)
            self.assertIn("profile", controls)
            self.assertIn("channel:thread", controls)
            self.assertIn("channel:posts-all", controls)
            self.assertIn("channel:edit-post", controls)
            self.assertIn("channel:delete", controls)

    def test_GET_post_404(self):
        '''
        Checks that GET post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_GET_post_404.__name__, self.test_GET_post_404.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))

        # Check that I receive status code 404
        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.get(api.url_for(Post, name="dev", thread_id=thread_id, post_id="58b08c489916d10f34f5f9d1"))
            self.assertEquals(resp.status_code, 404)


    def test_DELETE_post(self):
        '''
        Checks that DELETE Post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_DELETE_post.__name__, self.test_DELETE_post.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": thread_id,
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        post_id = mdb.create_post(thread_id, post)

        headers = {
          'Accept': 'application/vnd.mason+json'
        }


        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.delete(api.url_for(Post, name="dev", thread_id=thread_id, post_id=post_id),
                                    headers=headers)
            self.assertTrue(resp.status_code == 200)

            self.assertRaises(NameError, mdb.get_post, post_id)


    def test_DELETE_post_404(self):
        '''
        Checks that DELETE Post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_DELETE_post_404.__name__, self.test_DELETE_post_404.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))

        headers = {
          'Accept': 'application/vnd.mason+json'
        }


        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.delete(api.url_for(Post, name="dev", thread_id=thread_id, post_id="58b08c489916d10f34f5f9d1"),
                                    headers=headers)
            self.assertTrue(resp.status_code == 404)


    def test_PUT_post(self):
        '''
        Checks that PUT post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_PUT_post.__name__, self.test_PUT_post.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": thread_id,
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        post_id = mdb.create_post(thread_id, post)

        headers = {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.mason+json'
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.put(api.url_for(Post, name="dev", thread_id=thread_id, post_id=post_id),
                                headers=headers,
                                data=json.dumps(self.update_post)
                               )            
            self.assertEquals(resp.status_code, 200)
            url = resp.headers.get("Location")
            self.assertIsNotNone(url)
            resp = client.get(url)
            data = json.loads(resp.data)
            self.assertEqual(data["author"], "Arthur 2.0")


    def test_PUT_post_400(self):
        '''
        Checks that PUT post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_PUT_post.__name__, self.test_PUT_post.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": thread_id,
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        post_id = mdb.create_post(thread_id, post)

        headers = {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.mason+json'
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.put(api.url_for(Post, name="dev", thread_id=thread_id, post_id=post_id),
                                headers=headers,
                                data=json.dumps(self.update_post_400)
                               )
            self.assertEquals(resp.status_code, 400)

    def test_PUT_post_404(self):
        '''
        Checks that PUT post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_PUT_post_404.__name__, self.test_PUT_post_404.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))

        headers = {
          'Content-Type': 'application/json',
          'Accept': 'application/vnd.mason+json'
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.put(api.url_for(Post, name="dev", thread_id=thread_id, post_id="58b08c489916d10f34f5f9d1"),
                                headers=headers,
                                data=json.dumps(self.update_post)
                               )
            self.assertEquals(resp.status_code, 404)

    def test_PUT_post_415(self):
        '''
        Checks that PUT post return correct status code and data format
        '''
        print '({0}),{1}'.format(self.test_PUT_post_415.__name__, self.test_PUT_post_415.__doc__)

        #Creates the threads and post I need
        thread_id = mdb.create_thread("dev", dict(title="New Thread in DEV"))
        post = {
            "thread_id": thread_id,
            "author": "Arthur",
            "text": "I'm testing",
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "votes": "0",
            "references": [],
            "attachments": []
        }

        post_id = mdb.create_post(thread_id, post)

        headers = {
          'Content-Type': 'text',
          'Accept': 'application/vnd.mason+json'
        }

        with app.test_request_context(self.dev), app.test_client() as client:
            resp = client.put(api.url_for(Post, name="dev", thread_id=thread_id, post_id=post_id),
                                headers=headers,
                                data=json.dumps(self.update_post)
                               )
            self.assertEquals(resp.status_code, 415)

