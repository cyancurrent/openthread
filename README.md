# OpenThread
Anonymous forum engine, written with Python, Flask and MongoDB. 

Libraries required:

    1. pymongo
    2. flask
    3. flask_restful

## Database
Make sure you have MongoDB installed and `/bin` folder added to your PATH.

Start the database by calling `mongod --dbpath <directory>` in terminal. The `<directory>` is a path to database files. Don't store them in the project, since they take a lot of space.

Populate database with sample data, running `python populate.py` from root of the project (this also clears all existing content).

## Run
Launch application and populate with sampel data by calling `python run.py` from root of the project. The client application will be accessible by address `http://localhost:3000/`. Be aware, that client application also requires running database.

## Tests
Run specific tests cases with `python -m unittest test.<name of test file>.<name of test case>`.

Run all test cases in file with `python -m unittest test.<name of test file>`.

If you have [pytest](http://doc.pytest.org/en/latest/), run all tests with just `pytest`, which have nicer output.

Existing tests:

   1. Database tests - `python -m unittest test.test_database`
    * General - `python -m unittest test.test_database.GeneralTestCase`
    * Boards - `python -m unittest test.test_database.BoardTestCase`
    * Threads - `python -m unittest test.test_database.ThreadTestCase`
    * Posts - `python -m unittest test.test_database.PostTestCase`
   
   2. API test - `python -m unittest test.test_api`
    * Boards - `python -m unittest test.test_api.BoardsTestCase`
    * Threads - `python -m unittest test.test_api.ThreadsTestCase`
    * Posts - `python -m unittest test.test_api.PostsTestCase`
