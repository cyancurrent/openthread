'''
Created on 17.04.2017

Simple script to populate database with sample data.

@author: ilya
'''

from channel.common.database import Engine

if __name__ == '__main__':
    engine = Engine()
    engine.connect()
    engine.clear()
    engine.populate()
    engine.close()