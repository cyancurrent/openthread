'''
Created on 10.04.2017

Move application launch here to provide modular structure.

@author: ilya
'''

from channel import app

app.run('localhost', 3000, debug=True)