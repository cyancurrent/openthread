'''
Created on 20.02.2017

Core execution file, launches application and all processes.

@author: ilya
'''

import json
from flask import Flask, g, request, Response, _request_ctx_stack, redirect, send_from_directory
from flask_restful import Api

from channel.common.database import Engine
from channel.common.constants import *

app = Flask(__name__, static_folder="static", static_url_path="")
api = Api(app)
app.config.update({"Engine": Engine()})

# Errors handling
def create_error_response(status_code, title, message=None):
    """ 
    Creates a: py: class:`flask.Response` instance when sending back an
    HTTP error response

    : param integer status_code: The HTTP status code of the response
    : param str title: A short description of the problem
    : param message: A long description of the problem
    : rtype:: py: class:`flask.Response`
    """

    resource_url = None
    #We need to access the context in order to access the request.path
    ctx = _request_ctx_stack.top
    if ctx is not None:
        resource_url = request.path
    envelope = MasonObject(resource_url=resource_url)
    envelope.add_error(title, message)

    return Response(json.dumps(envelope), status_code, mimetype=MASON+";"+ERROR_PROFILE)

# Import resources after to avoid circular imports
from channel.resources.boards import Boards, Board
from channel.resources.threads import Threads, Thread
from channel.resources.posts import Posts, Post
from channel.common.mason import MasonObject

@app.errorhandler(404)
def resource_not_found(error):
    return create_error_response(404, "Resource not found",
                                 "This resource url does not exit")

@app.errorhandler(400)
def resource_not_found(error):
    return create_error_response(400, "Malformed input format",
                                 "The format of the input is incorrect")

@app.errorhandler(500)
def unknown_error(error):
    return create_error_response(500, "Error",
                    "The system has failed. Please, contact the administrator")

# Hooks
@app.before_request
def connect_db():
    """
    Creates a database connection before the request is proccessed.

    The connection is stored in the application context variable flask.g .
    Hence it is accessible from the request object.
    """

    g.db = app.config["Engine"].connect()
def make_session_permanent():
    session.permanent = True

@app.teardown_request
def close_connection(exc):
    """ 
    Closes the database connection
    Check if the connection is created. It migth be exception appear before
    the connection is created.
    """

    if hasattr(g, "db"):
        g.db.close()


@app.route('/')
def index():
    return send_from_directory(app.static_folder, 'index.html')


# Define resources
api.add_resource(Boards, "/channel/api/boards/", endpoint="boards")
api.add_resource(Board, "/channel/api/boards/<name>/", endpoint="board")
api.add_resource(Threads, "/channel/api/boards/<name>/threads/", endpoint="threads")
api.add_resource(Thread, "/channel/api/boards/<name>/threads/<thread_id>/", endpoint="thread")
api.add_resource(Posts, "/channel/api/boards/<name>/threads/<thread_id>/posts/", endpoint="posts")
api.add_resource(Post, "/channel/api/boards/<name>/threads/<thread_id>/posts/<post_id>/", endpoint="post")