'''
Created on 10.04.2017

Contains definition of Channel constants, primerely links.

@author: ilya
'''

MASON = "application/vnd.mason+json"
JSON = "application/json"

LINK_RELATIONS_URL = "http://docs.openthreadapi.apiary.io/reference/link-relations"
CHANNEL_BOARD_PROFILE = "http://docs.openthreadapi.apiary.io/reference/profiles/board-profile"
CHANNEL_THREAD_PROFILE = "http://docs.openthreadapi.apiary.io/reference/profiles/thread-profile"
CHANNEL_POST_PROFILE = "http://docs.openthreadapi.apiary.io/reference/profiles/post-profile"
ERROR_PROFILE = "http://docs.openthreadapi.apiary.io/reference/profiles/error-profile"