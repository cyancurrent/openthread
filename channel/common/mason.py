'''
Created on 02.04.2017

Defines abstract MasonObject and OpenThread specific ChannelObject.

@author: ilya
'''

from channel import api


class MasonObject(dict):
    """
    A convenience class for managing dictionaries that represent Mason
    objects. It provides nice shorthands for inserting some of the more
    elements into the object but mostly is just a parent for the much more
    useful subclass defined next. This class is generic in the sense that it
    does not contain any application specific implementation details.
    """

    def add_error(self, title, details):
        """
        Adds an error element to the object. Should only be used for the root
        object, and only in error scenarios.

        Note: Mason allows more than one string in the @messages property (it's
        in fact an array). However we are being lazy and supporting just one
        message.

        : param str title: Short title for the error
        : param str details: Longer human-readable description
        """

        self["@error"] = {
            "@message": title,
            "@messages": [details],
        }

    def add_namespace(self, ns, uri):
        """
        Adds a namespace element to the object. A namespace defines where our
        link relations are coming from. The URI can be an address where
        developers can find information about our link relations.

        : param str ns: the namespace prefix
        : param str uri: the identifier URI of the namespace
        """

        if "@namespaces" not in self:
            self["@namespaces"] = {}

        self["@namespaces"][ns] = {
            "name": uri
        }

    def add_control(self, ctrl_name, **kwargs):
        """
        Adds a control property to an object. Also adds the @controls property
        if it doesn't exist on the object yet. Technically only certain
        properties are allowed for kwargs but again we're being lazy and don't
        perform any checking.

        The allowed properties can be found from here
        https://github.com/JornWildt/Mason/blob/master/Documentation/Mason-draft-2.md

        : param str ctrl_name: name of the control (including namespace if any)        
        """

        if "@controls" not in self:
            self["@controls"] = {}

        self["@controls"][ctrl_name] = kwargs


class ChannelObject(MasonObject):
    """
    A convenience subclass of MasonObject that defines a bunch of shorthand 
    methods for inserting application specific objects into the document. This
    class is particularly useful for adding control objects that are largely
    context independent, and defining them in the resource methods would add a 
    lot of noise to our code - not to mention making inconsistencies much more
    likely!

    In the channel code this object should always be used for root document as 
    well as any items in a collection type resource. 
    """

    def __init__(self, **kwargs):
        """
        Calls dictionary init method with any received keyword arguments. Adds
        the controls key afterwards because hypermedia without controls is not 
        hypermedia. 
        """

        super(ChannelObject, self).__init__(**kwargs)
        self["@controls"] = {}

    def add_control_boards_all(self):
        """
        Adds the boards-all link to an object. Intended for the document object.
        """

        self["@controls"]["channel:boards-all"] = {
            "href": api.url_for(Boards),
            "method": "GET"
        }

    def add_control_threads_all(self, name):
        """
        Adds the threads-all link to an object. Intended for the document object.
        """

        self["@controls"]["channel:threads-all"] = {
            "href": api.url_for(Threads, name=name),
            "method": "GET"
        }

    def add_control_posts_all(self, name, thread_id):
        """
        Adds the posts-all link to an object. Intended for the document object.
        """

        self["@controls"]["channel:posts-all"] = {
            "href": api.url_for(Posts, name=name, thread_id=thread_id),
            "method": "GET"
        }

    def add_control_create_board(self):
        """
        This adds the create-board control to an object. Intended for the  
        document object.
        """

        self["@controls"]["channel:create-board"] = {
            "href": api.url_for(Boards),
            "title": "Create board",
            "encoding": "json",
            "method": "POST",
            "schema": self._board_schema()
        }

    def add_control_create_thread(self, name):
        """
        This adds the board control to an object. Intended for the
        document object.
        """
        self["@controls"]["channel:create-thread"] = {
            "href": api.url_for(Threads, name=name),
            "title": "Create thread",
            "encoding": "json",
            "method": "POST",
            "schema": self._thread_schema()
        }

    def add_control_create_post(self, name, thread_id):
        """
        This adds the thread control to an object. Intended for the  
        document object.
        """       
        self["@controls"]["channel:create-post"] = {
            "href": api.url_for(Posts, name = name, thread_id = thread_id),
            "title": "Create Post",
            "encoding": "json",
            "method": "POST",
            "schema": self._post_schema()
        }

    def add_control_edit_board(self, name):
        """
        This adds the edit-board control to an object. Intended for the  
        document object.
        """

        self["@controls"]["channel:edit-board"] = {
            "href": api.url_for(Board, name=name),
            "title": "Edit board",
            "encoding": "json",
            "method": "PUT",
            "schema": self._board_schema()
        }

    def add_control_edit_thread(self, name, thread_id):
        """
        This adds the edit-thread control to an object. Intended for the
        document object.
        """
        self["@controls"]["channel:edit-thread"] = {
            "href": api.url_for(Thread, name=name, thread_id=thread_id),
            "title": "Edit thread",
            "encoding": "json",
            "method": "PUT",
            "schema": self._thread_schema()
        }

    def add_control_edit_post(self, name, thread_id, post_id):
        """
        This adds the edit-post control to an object. Intended for the  
        document object.
        """

        self["@controls"]["channel:edit-post"] = {
            "href": api.url_for(Post, name=name, thread_id=thread_id, post_id=post_id),
            "title": "Edit post",
            "encoding": "json",
            "method": "PUT",
            "schema": self._edit_post_schema()
        }

    def add_control_delete_board(self, name):
        """
        Adds the delete control to an object of board. This is intended for any 
        object that represents a message.

        :param name: Name of the requested board
        :type name: str
        """

        self["@controls"]["channel:delete"] = {
            "href": api.url_for(Board, name=name),
            "method": "DELETE"
        }

    def add_control_delete_thread(self, name, thread_id):
        """
        Adds the delete control to an object of Thread. This is intended for any 
        object that represents a message.

        :param name: Name of the requested board
        :param thread_id: Id of the requested thread
        :type name: str
        """

        self["@controls"]["channel:delete"] = {
            "href": api.url_for(Thread, name=name, thread_id=thread_id),
            "method": "DELETE"
        }

    def add_control_delete_post(self, name, thread_id, post_id):
        """
        Adds the delete control to an object of board. This is intended for any 
        object that represents a message.

        :param name: Name of the requested board
        :param thread_id: Id of the requested thread
        :param post_id: Id of the requested post
        :type name: str
        """

        self["@controls"]["channel:delete"] = {
            "href": api.url_for(Post, name=name, thread_id=thread_id, post_id=post_id),
            "method": "DELETE"
        }

    def add_control_board(self, name):
        """
        This adds the board control to an object. Intended for the
        document object.
        """
        self["@controls"]["channel:board"] = {
            "href": api.url_for(Board, name=name),
            "method": "GET"
        }

    def add_control_thread(self, name, thread_id):
        """
        This adds the thread control to an object. Intended for the  
        document object.
        """       
        self["@controls"]["channel:thread"] = {
            "href": api.url_for(Thread, name = name, thread_id = thread_id),
            "method": "GET"
        }


    def _board_schema(self):
        """
        Creates a schema dictionary for board.

        :return: The dict containing board schema.
        """

        schema = {
            "type": "object",
            "properties": {},
            "required": ["name"]
        }

        props = schema["properties"]
        props["name"] = {
            "title": "Name",
            "description": "Board name",
            "type": "string"
        }
        props["description"] = {
            "title": "Description",
            "description": "Board description",
            "type": "string"
        }
        return schema

    def _thread_schema(self):
        """
        Creates a schema dictionary for board.

        :return: The dict containing thread schema.
        """

        schema = {
            "type": "object",
            "properties": {},
            "required": ["title"]
        }

        props = schema["properties"]
        props["title"] = {
            "title": "Title",
            "description": "Title of the thread",
            "type": "string"
        }
        return schema

    def _post_schema(self):
        """
        Creates a schema dictionary for post.

        :return: The dict containing post schema
        """
        schema = {
            "type": "object",
            "properties": {},
            "required": ["text"]
        }

        props = schema["properties"]
        props["text"] = {
            "title": "Text",
            "description": "Post text",
            "type": "string"
        }
        props["author"] = {
            "title": "Author",
            "description": "Post author",
            "type": "string"
        }
        props["votes"] = {
            "title": "Votes",
            "description": "Number of votes",
            "type": "number"
        }
        props["references"] = {
            "title": "References",
            "description": "Post references",
            "type": "array"
        }
        props["attachments"] = {
            "title": "Attachments",
            "description": "Post attachments",
            "type": "array"
        }
        return schema

    def _edit_post_schema(self):
        schema = {
            "type": "object",
            "properties": {},
            "required": ["text"]
        }

        props = schema["properties"]
        props["text"] = {
            "title": "Text",
            "description": "Post text",
            "type": "string"
        }
        props["votes"] = {
            "title": "Votes",
            "description": "Number of votes",
            "type": "number"
        }
        return schema



from channel.resources.boards import Boards, Board
from channel.resources.threads import Threads, Thread
from channel.resources.posts import Posts, Post