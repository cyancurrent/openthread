'''
Created on 20.02.2017

Provides the database API to access the board persistent data.

@author: ilya
'''

from pymongo import MongoClient
from bson.objectid import ObjectId
import os
import json
import subprocess
import datetime

# Python recognizes dirs relative to launch file
SAMPLES = 'sample/data/'


class Engine(object):

    def __init__(self, db_name='channel'):
        super(Engine, self).__init__()
        self.mdb = None
        self.db_name = db_name

    def startDB(self, db_path):
        '''
        Don't use this!
        '''
        mongod = subprocess.Popen(
            ['mongod', '--dbpath', os.path.expanduser(db_path)])

    def connect(self, host='localhost', port=27017):
        '''
        Connects to the MongoDB instance

        :param host: default localhost. Specific host
        :type host: str
        :param port: default 27017. Specific port
        :type port: int
        '''
        self.mdb = MongoClient(host=host, port=port)
        return self

    def close(self):
        '''
        Closes active connection to MongoDB instance
        '''
        if self.mdb is not None:
            self.mdb.close()

    def populate(self):
        '''
        Populate db wiht sample data
        '''
        chan = self.mdb[self.db_name]
        # Populate with boards
        boards = chan['boards']
        boards_json = open(SAMPLES + 'boards.json', 'r')
        boards_parsed = json.loads(boards_json.read())
        for item in boards_parsed:
            self.create_board(item['name'])
        # Populate with threads
        threads = chan['threads']
        threads_json = open(SAMPLES + 'threads.json', 'r')
        threads_parsed = json.loads(threads_json.read())
        for item in threads_parsed:
            thread_id = self.create_thread(item['board'], item)
            # Populate threads with posts
            for post in item['posts']:
                self.create_post(thread_id, post)

    def clear(self):
        '''
        Clear all documents in db
        '''
        chan = self.mdb[self.db_name]
        boards = chan['boards']
        boards.delete_many({})
        threads = chan['threads']
        threads.delete_many({})
        posts = chan['posts']
        posts.delete_many({})

    def create_board(self, name, description=""):
        '''
        Create board with given name. The name must be unique.

        :param name: The name of board, conventionally format is /name
        :type host: str

        :return: The id of created board or None is board with given name exists
        '''
        chan = self.mdb[self.db_name]
        boards = chan['boards']
        # Find existing board
        cursor = boards.find_one({"name": name})
        if cursor is None:
            # If none found, create and return id
            result = boards.insert_one({
                "name": name,
                "description": description,
                "created_utc": datetime.datetime.utcnow(),
                "edited_utc": datetime.datetime.utcnow(),
                "threads": []
            })
            return result.inserted_id
        else:
            # Else return None
            return None

    def get_all_boards(self):
        '''
        Retrieve all boards.

        :return: Array of board data

        :raises NameError: If given board was not found.
        '''
        chan = self.mdb[self.db_name]
        boards = chan['boards']
        cursor = boards.find()
        return cursor

    def get_board(self, name):
        '''
        Retrieve board with given name.

        :param name: The name of board, conventionally format is /name
        :type host: str

        :return: The dict with board data

        :raises NameError: If given board was not found.
        '''
        chan = self.mdb[self.db_name]
        boards = chan['boards']
        # Find existing board
        cursor = boards.find_one({"name": name})
        if cursor is None:
            raise NameError("Board {0} is not found".format(name))
        else:
            return cursor

    def edit_board(self, name, data):
        '''
        Edit board information.

        :param name: The name of board, conventionally format is /name
        :type name: str

        :param data: New details
        :type data: dict

        :return: True if board was modified, False otherwise

        :raises NameError: If given board was not found.
        '''
        chan = self.mdb[self.db_name]
        boards = chan['boards']
        cursor = boards.find_one({"name": name})
        if cursor is None:
            raise NameError("Board {0} is not found".format(name))
        edits = dict(data)
        # Removing fields that must not be modified
        edits.pop("name", None)
        edits.pop("created_utc", None)
        edits.pop("edited_utc", None)
        edits.pop("threads", None)
        # Update
        upd = boards.update_one({"name": name}, {"$set": edits, "$currentDate": {"edited_utc": True}})
        if upd.modified_count > 0:
            return True
        else:
            return False

    def delete_board(self, name):
        '''
        Removes board with given name. Warning, also removes all child threads and posts!

        :param name: The name of board, conventionally format is /name
        :type host: str

        :return: True if board was successfully removed or False if operation failed

        :raises NameError: If given board was not found.
        '''
        chan = self.mdb[self.db_name]
        boards = chan['boards']
        cursor = boards.find_one({"name": name})
        if cursor is None:
            raise NameError("Board {0} is not found".format(name))
        else:
            # Remove child threads
            for thread_id in cursor['threads']:
                self.delete_thread(thread_id)
            result = boards.delete_one({'_id': cursor['_id']})
            if result.deleted_count > 0:
                return True
        return False

    def create_thread(self, board, thread_dict):
        '''
        Create thread with given title under give board.

        :param board: The name of board, conventionally format is /name
        :type host: str
        :param title: The title of new thread
        :type host: str

        :return: The id of created thread

        :raises NameError: If given board was not found.
        '''
        chan = self.mdb[self.db_name]
        boards = chan['boards']
        # Find existing board
        cursor = boards.find_one({"name": board})
        if cursor is None:
            raise NameError("Board {0} is not found".format(board))
        next_thr = len(cursor['threads'])
        threads = chan['threads']
        result = threads.insert_one({
            "board": board,
            "title": thread_dict["title"],
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "posts": []
        })
        boards.update_one({"name": board}, {
                          '$push': {'threads': result.inserted_id}})
        return result.inserted_id

    def get_thread(self, thread_id):
        '''
        Extracts a thread from the database.

        :param thread_id: The id of the thread.

        :return: The thread with the given id.

        :raises NameError: If given thread was not found.
        '''
        if ObjectId.is_valid(thread_id):
            chan = self.mdb[self.db_name]
            threads = chan['threads']
            cursor = threads.find_one({"_id": ObjectId(thread_id)})
            if cursor is None:
                raise NameError("Thread is not found")
            return cursor
        raise TypeError("param thread_id does not have the right type")

    def edit_thread(self, thread_id, data):
        '''
        Edit thread information.

        :param thread_id: Thread ID
        :type thread_id: str

        :param data: New details
        :type data: dict

        :return: True if thread was modified, False otherwise

        :raises NameError: If given thread was not found.
        '''
        chan = self.mdb[self.db_name]
        threads = chan['threads']
        cursor = threads.find_one({"_id": ObjectId(thread_id)})
        if cursor is None:
            raise NameError("Thread {0} is not found".format(thread_id))
        edits = dict(data)
        # Removing fields that must not be modified
        edits.pop("board", None)
        edits.pop("created_utc", None)
        edits.pop("edited_utc", None)
        edits.pop("posts", None)
        # Update
        upd = threads.update_one({"_id": ObjectId(thread_id)}, {"$set": edits, "$currentDate": {"edited_utc": True}})
        if upd.modified_count > 0:
            return True
        else:
            return False

    def delete_thread(self, thread_id):
        '''
        Delete thread with given id. Warning, also removes posts in this thread.

        :param thread_id: id of the thread to be removed.

        :return: True if the thread has been deleted

        :raises NameError: if the thread_id was not found.
        '''
        if ObjectId.is_valid(thread_id):
            chan = self.mdb[self.db_name]
            threads = chan['threads']
            cursor = threads.find_one({"_id": ObjectId(thread_id)})
            if cursor is None:
                raise NameError("Thread {0} is not found".format(thread_id))
            # Remove posts as well
            for post_id in cursor['posts']:
                self.delete_post(post_id)
            # Finally remove thread itself
            result = threads.delete_one({"_id": ObjectId(thread_id)})
            if result.deleted_count > 0:
                return True
            return False
        raise TypeError("param thread_id does not have the right type")

    def create_post(self, thread_id, post_dict):
        '''
        Create post with under give thread.

        :param thread_id: The id of parent thread
        :type host: str
        :param post_dict: The dict containing information about post 
        :type host: dict

        :return: The id of created post

        :raises NameError: If given thread was not found.
        '''
        chan = self.mdb[self.db_name]
        threads = chan['threads']
        cursor = threads.find_one({"_id": ObjectId(thread_id)})
        if cursor is None:
            raise NameError("Thread with _id {0} is not found".format(thread_id))
        next_post = len(cursor['posts'])
        posts = chan['posts']
        result = posts.insert_one({
            "thread_id": ObjectId(thread_id),
            "author": post_dict.get('author', 'Anonymous'),
            "text": post_dict['text'],
            "created_utc": datetime.datetime.utcnow(),
            "edited_utc": datetime.datetime.utcnow(),
            "votes": post_dict.get('votes', 0),
            "references": post_dict.get('references', []),
            "attachments": post_dict.get('attachments', [])
        })
        threads.update_one({"_id": ObjectId(thread_id)}, {
            '$push': {'posts': result.inserted_id}})
        return result.inserted_id

    def get_post(self, post_id):
        '''
        Retrieve post with given id

        :param post_id: The id of the post.

        :return: The post with the given id.

        :raises NameError: If given post was not found.
        '''
        if ObjectId.is_valid(post_id):
            chan = self.mdb[self.db_name]
            posts = chan['posts']
            cursor = posts.find_one({"_id": ObjectId(post_id)})
            if cursor is None:
                raise NameError("Post is not found")
            return cursor
        raise TypeError("param post_id does not have the right type")

    def edit_post(self, post_id, post_dict):
        '''
        Edit post information.

        :param post_id: The ID of the post
        :type post_id: str

        :param post_dict: New text or upvote
        :type data: dict

        :return: True if board was modified, False otherwise

        :raises NameError: If given board was not found.
        '''
        chan = self.mdb[self.db_name]
        posts = chan['posts']
        cursor = posts.find_one({"_id": ObjectId(post_id)})
        if cursor is None:
            raise NameError("Post {0} is not found".format(post_id))
        edits = dict(post_dict)
        # Removing fields that must not be modified
        edits.pop("thread_id", None)
        edits.pop("votes", None)
        edits.pop("created_utc", None)
        edits.pop("references", None)
        edits.pop("attachments", None)
        # Update
        upd = posts.update_one({"_id": ObjectId(post_id)}, {"$set": edits, "$currentDate": {"edited_utc": True}})
        if upd.modified_count > 0:
            return True
        else:
            return False

    def delete_post(self, post_id):
        '''
        Delete post with given id

        :param post_id: id of the post to be removed.

        :return: True if the post has been deleted

        :raises NameError: if the post_id was not found.
        '''
        if ObjectId.is_valid(post_id):
            chan = self.mdb[self.db_name]
            posts = chan['posts']
            cursor = posts.find_one({"_id": ObjectId(post_id)})
            if cursor is None:
                raise NameError("Post {0} is not found".format(post_id))
            result = posts.delete_one({"_id": ObjectId(post_id)})
            if result.deleted_count > 0:
                return True
            return False
        raise TypeError("param post_id does not have the right type")
