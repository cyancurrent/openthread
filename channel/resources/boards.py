'''
Created on 02.04.2017

Contains definition of Boards related resources.

@author: ilya
'''

import json
from bson import json_util
from flask import g, request, Response
from flask_restful import Resource

from channel import api, create_error_response

from channel.common.constants import *


class Boards(Resource):

    def get(self):
        '''
        Get list of available boards

        :return: return something
        '''
        envelope = ChannelObject()

        envelope.add_namespace("channel", LINK_RELATIONS_URL)
        envelope.add_control("self", href=api.url_for(Boards))
        envelope.add_control("profile", href=CHANNEL_BOARD_PROFILE)
        envelope.add_control_create_board()

        items = envelope["items"] = []
        boards = g.db.get_all_boards()
        for board in boards:
            item = ChannelObject(name=board["name"])
            item["name"] = board["name"]
            item["description"] = board["description"]
            item["created_utc"] = board["created_utc"]
            item["edited_utc"] = board["edited_utc"]
            item.add_control("self", href=api.url_for(Board, name=board["name"]))
            item.add_control("profile", href=CHANNEL_BOARD_PROFILE)
            items.append(item)
        return Response(json.dumps(envelope, default=json_util.default), 200, mimetype=MASON + ";" + CHANNEL_BOARD_PROFILE)

    def post(self):
        '''
        Create a new board

        :return: return something
        '''
        if JSON != request.headers.get("Content-Type",""):
            return create_error_response(415, "UnsupportedMediaType",
                                         "Use a JSON compatible format")
        payload = request.get_json(force=True)
        # Required arguments
        try:
            name = payload["name"]
        except KeyError:
            return create_error_response(400, "Wrong request format", "Be sure you include name in payload")
        # Optional arguments
        description = payload.get("description", "")
        result = g.db.create_board(name, description)
        if result is None:
            return create_error_response(500, "Uniqueness violation", "Board with given name already exist")
        else:
            url = api.url_for(Board, name=name)
            return Response(status=200, headers={"Location": url})


class Board(Resource):

    def get(self, name):
        '''
        Get info about this board

        :param name: Name of the requested board
        :type unknown: str

        :return: return something
        '''
        try:
            board = g.db.get_board(name)
        except NameError:
            return create_error_response(404, "Not found", "Board {0} does not exist".format(name))

        envelope = ChannelObject()
        envelope.add_namespace("channel", LINK_RELATIONS_URL)

        # Controls
        envelope.add_control("self", href=api.url_for(Board, name=name))
        envelope.add_control("profile", href=CHANNEL_BOARD_PROFILE)
        envelope.add_control_boards_all()
        envelope.add_control_edit_board(name)
        envelope.add_control_delete_board(name)
        envelope.add_control_threads_all(name)

        # Actual board data
        envelope["name"] = board["name"]
        envelope["description"] = board["description"]
        envelope["created_utc"] = board["created_utc"]
        envelope["edited_utc"] = board["edited_utc"]

        return Response(json.dumps(envelope, default=json_util.default), 200, mimetype=MASON + ";" + CHANNEL_BOARD_PROFILE)

    def put(self, name):
        '''
        Modifies the info about board

        :param name: Name of the requested board
        :type unknown: str

        :return: return something
        '''
        if JSON != request.headers.get("Content-Type",""):
            return create_error_response(415, "UnsupportedMediaType",
                                         "Use a JSON compatible format")
        try:
            payload = request.get_json(force=True)
            updated = g.db.edit_board(name, payload)
        except NameError:
            return create_error_response(404, "Not found", "Board {0} does not exist".format(name))
        if updated:
            url = api.url_for(Board, name=name)
            return Response(status=200, headers={"Location": url}, response="Board was modified successfully")

    def delete(self, name):
        '''
        Delete this board and all content inside

        :param name: Name of the requested board
        :type unknown: str

        :return: return something
        '''
        try:
            deleted = g.db.delete_board(name)
        except NameError:
            return create_error_response(404, "Not found", "Board {0} does not exist".format(name))
        if deleted:
            return Response(status=200, response="Board was removed successfully")

from channel.common.mason import MasonObject, ChannelObject