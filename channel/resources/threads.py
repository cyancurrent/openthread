'''
Created on 02.04.2017

Contains definition of Threads related resources.

@author: ilya
'''

import json
from bson import json_util
from flask import g, request, Response
from flask_restful import Resource

from channel import api, create_error_response

from channel.common.constants import *


class Threads(Resource):

    def get(self, name):
        '''
        Get list of all threads in this board

        :param name: Name of the parent board
        :type unknown: str

        :return: return something
        '''
        try:
            board = g.db.get_board(name)
        except NameError:
           return create_error_response(404, "Not found", "Board {0} does not exist".format(name))

        envelope = ChannelObject()

        envelope.add_namespace("channel", LINK_RELATIONS_URL)
        envelope.add_control("self", href=api.url_for(Threads, name=name))
        envelope.add_control_board(name)
        envelope.add_control_create_thread(name)

        items = envelope["items"] = []

        for thread_id in board["threads"]:
            try:
                thread = g.db.get_thread(thread_id)
                item = ChannelObject(board=thread["board"],
                                    title=thread["title"],
                                    created_utc=thread["created_utc"],
                                    edited_utc=thread["edited_utc"],
                                    thread_id=str(thread["_id"])
                                    )
                item.add_control("self", href=api.url_for(Thread, name=name, thread_id=thread["_id"]))
                item.add_control("profile", href=CHANNEL_THREAD_PROFILE)
                items.append(item)
            except NameError:
                pass
        return Response(json.dumps(envelope, default=json_util.default), 200, mimetype=MASON + ";" + CHANNEL_THREAD_PROFILE)

    def post(self, name):
        '''
        Create a new thread in this board

        :param name: Name of the parent board
        :type unknown: str

        :return: return something
        '''
        try:
            board = g.db.get_board(name)
        except NameError:
            return create_error_response(404, "Not found", "Board {0} does not exist".format(name))

        if JSON != request.headers.get("Content-Type", ""):
            return create_error_response(415, "UnsupportedMediaType",
                                         "Use a JSON compatible format")
        payload = request.get_json(force=True)
        # Required arguments
        try:
            title = payload["title"]
        except KeyError:
            return create_error_response(400, "Wrong request format", "Be sure you include text in payload")

        thread_id = g.db.create_thread(name, payload)
        url = api.url_for(Thread, name=name, thread_id=thread_id)
        return Response(status=200, headers={"Location": url})


class Thread(Resource):

    def get(self, name, thread_id):
        '''
        Get info about this thread

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the requested thread
        :type thread_id: str

        :return: return something
        '''

        try:
            thread = g.db.get_thread(thread_id)
        except NameError:
            return create_error_response(404, "Not found", "Thread {0} does not exist".format(thread_id))

        envelope = ChannelObject()
        envelope.add_namespace("channel", LINK_RELATIONS_URL)

        # Controls
        envelope.add_control("self", href=api.url_for(Thread, name=name, thread_id=thread_id))
        envelope.add_control("profile", href=CHANNEL_THREAD_PROFILE)
        envelope.add_control_board(name)
        envelope.add_control_threads_all(name)
        envelope.add_control_posts_all(name, thread_id)
        envelope.add_control_edit_thread(name, thread_id)
        envelope.add_control_delete_thread(name, thread_id)

        # Actual thread data
        envelope["board"] = name
        envelope["thread_id"] = str(thread["_id"])
        envelope["title"] = thread["title"]
        envelope["created_utc"] = thread["created_utc"]
        envelope["edited_utc"] = thread["edited_utc"]

        return Response(json.dumps(envelope, default=json_util.default), 200, mimetype=MASON + ";" + CHANNEL_THREAD_PROFILE)

    def put(self, name, thread_id):
        '''
        Modifies the info about thread

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the requested thread
        :type thread_id: str

        :return: return something
        '''
        if JSON != request.headers.get("Content-Type", ""):
            return create_error_response(415, "UnsupportedMediaType",
                                         "Use a JSON compatible format")
        payload = request.get_json(force=True)
        # Required arguments
        try:
            title = payload["title"]
        except KeyError:
            return create_error_response(400, "Wrong request format", "Be sure you include text in payload")

        try:
            updated = g.db.edit_thread(thread_id, payload)
        except NameError:
            return create_error_response(404, "Not found", "Thread {0} does not exist".format(thread_id))
        if updated:
            url = api.url_for(Thread, name=name, thread_id=thread_id)
            return Response(status=200, headers={"Location": url}, response="Threads was modified successfully")

    def delete(self, name, thread_id):
        '''
        Delete this thread and all content inside

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the requested thread
        :type thread_id: str

        :return: return something
        '''
        try:
            deleted = g.db.delete_thread(thread_id)
        except NameError:
            return create_error_response(404, "Not found", "Post {0} does not exist".format(thread_id))
        if deleted:
            return Response(status=200, response="Thread was removed successfully")

from channel.common.mason import MasonObject, ChannelObject