'''
Created on 02.04.2017

Contains definition of Posts related resources.

@author: ilya
'''

import json
from bson import json_util
from flask import g, request, Response
from flask_restful import Resource

from channel import api, create_error_response

from channel.common.constants import *


class Posts(Resource):

    def get(self, name, thread_id):
        '''
        Get list of posts in this thread

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the parent thread
        :type thread_id: str

        :return: return something
        '''
        try:
            thread = g.db.get_thread(thread_id)
        except NameError:
            return create_error_response(404, "Not found", "Thread {0} does not exist".format(thread_id))

        envelope = ChannelObject()
        envelope.add_namespace("channel", LINK_RELATIONS_URL)

        envelope.add_control("self", href=api.url_for(Posts, name= name, thread_id = thread_id))
        envelope.add_control_thread(name, thread_id)
        envelope.add_control_create_post(name, thread_id)

        items = envelope["items"] = []

        for post_id in thread["posts"]:
            try:   
                post = g.db.get_post(post_id)          
                item = ChannelObject(thread_id=thread_id,
                                     post_id=str(post["_id"]),
                                     text=post["text"],
                                     author=post["author"],
                                     created_utc=post["created_utc"],
                                     edited_utc=post["edited_utc"],
                                     votes=post["votes"],
                                     references=post["references"],
                                     attachments=post["attachments"] )

                item.add_control("self", href=api.url_for(Post, name=name, thread_id=thread_id, post_id=post["_id"]))
                item.add_control("profile", href=CHANNEL_POST_PROFILE)
                items.append(item)
            except NameError:
                pass
        #RENDER
        return Response(json.dumps(envelope, default=json_util.default), 200, mimetype=MASON + ";" + CHANNEL_THREAD_PROFILE)

    def post(self, name, thread_id):
        '''
        Create a new post in this thread

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the parent thread
        :type thread_id: str

        :return: return something
        '''
        try:
            board = g.db.get_board(name)
        except NameError:
            return create_error_response(404, "Not found", "Board {0} does not exist".format(name))

        try:
            thread = g.db.get_thread(thread_id)
        except NameError:
            return create_error_response(404, "Not found", "Thread {0} does not exist".format(thread_id))

        if JSON != request.headers.get("Content-Type",""):
            return create_error_response(415, "UnsupportedMediaType",
                                         "Use a JSON compatible format")
        payload = request.get_json(force=True)
        # Required arguments
        try:
            text = payload["text"]
        except KeyError:
            return create_error_response(400, "Wrong request format", "Be sure you include text in payload")

        post_id = g.db.create_post(thread_id, payload)

        url = api.url_for(Post, name=name, thread_id=thread_id, post_id=post_id)
        return Response(status=200, headers={"Location": url})


class Post(Resource):

    def get(self, name, thread_id, post_id):
        '''
        Get info about this post

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the parent thread
        :type thread_id: str
        :param post_id: ID of the requested post
        :type post_id: str

        :return: return something
        '''
        try:
            test_board = g.db.get_board(name)
        except NameError:
            return create_error_response(404, "Not found", "Board {0} does not exist".format(name))

        try:
            test_thread = g.db.get_thread(thread_id)
        except NameError:
            return create_error_response(404, "Not found", "Thread {0} does not exist".format(thread_id))

        try:
            post = g.db.get_post(post_id)
        except NameError:
            return create_error_response(404, "Not found", "Post {0} does not exist".format(post_id))

        envelope = ChannelObject()
        envelope.add_namespace("channel", LINK_RELATIONS_URL)

        # Actual Post data
        envelope["thread_id"] = post["thread_id"]
        envelope["post_id"] = str(post["_id"])
        envelope["text"] = post["text"]
        envelope["author"] = post["author"]
        envelope["votes"] = post["votes"]
        envelope["created_utc"] = post["created_utc"]
        envelope["edited_utc"] = post["edited_utc"]
        envelope["references"] = post["references"]
        envelope["attachments"] = post["attachments"]

        # Controls
        envelope.add_control("self", href=api.url_for(Post, name=name, thread_id=thread_id, post_id=post_id))
        envelope.add_control("profile", href=CHANNEL_POST_PROFILE)
        envelope.add_control_thread(name, thread_id)
        envelope.add_control_posts_all(name, thread_id)
        envelope.add_control_edit_post(name, thread_id, post_id)
        envelope.add_control_delete_post(name, thread_id, post_id)

        return Response(json.dumps(envelope, default=json_util.default), 200, mimetype=MASON + ";" + CHANNEL_BOARD_PROFILE)


    def put(self, name, thread_id, post_id):
        '''
        Modify this post

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the parent thread
        :type thread_id: str
        :param post_id: ID of the requested post
        :type post_id: str

        :return: return something
        '''
        if JSON != request.headers.get("Content-Type",""):
            return create_error_response(415, "UnsupportedMediaType",
                                         "Use a JSON compatible format")
        payload = request.get_json(force=True)
        # Required arguments
        try:
            text = payload["text"]
        except KeyError:
            return create_error_response(400, "Wrong request format", "Be sure you include text in payload")

        try:
            updated = g.db.edit_post(post_id, payload)
        except NameError:
            return create_error_response(404, "Not found", "Post {0} does not exist".format(name))
        if updated:
            url = api.url_for(Post, name=name, thread_id=thread_id, post_id=post_id)
            return Response(status=200, headers={"Location": url}, response="Post was modified successfully")


    def delete(self, name, thread_id, post_id):
        '''
        Delete this post

        :param name: Name of the parent board
        :type unknown: str
        :param thread_id: ID of the parent thread
        :type thread_id: str
        :param post_id: ID of the requested post
        :type post_id: str

        :return: return something
        '''
        try:
            deleted = g.db.delete_post(post_id)
        except NameError:
            return create_error_response(404, "Not found", "Post {0} does not exist".format(post_id))
        if deleted:
            return Response(status=200, response="Post was removed successfully")


from channel.common.mason import MasonObject, ChannelObject